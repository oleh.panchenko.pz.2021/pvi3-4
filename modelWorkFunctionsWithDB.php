<?php
function getStudentsData()
{
    global $db;
    $data = $db->query("SELECT * FROM students");

    $students = [];
    foreach($data as $record)
    {
        $student = [
            "id" => $record["id"],
            "studentGroup" => $record["studentGroup"],
            "name" => $record["name"],
            "gender" => $record["gender"],
            "birthday" => $record["birthday"],
            "status" => $record["status"]
        ];
        $students[] = $student;
    }
    $res = ["students" => $students, "hasAnyErrors" => false];
    return $res;
}

function deleteStudentByID($id)
{
    global $db;
    $sth = $db->prepare("DELETE FROM students WHERE id = :id");
    $sth->execute(['id' => $id]);
    return ["hasAnyErrors" => false];
}
function insertStudent($group, $firstName, $lastName, $birthday, $gender)
{
    try {
        global $db;
        $sth = $db->prepare("INSERT IGNORE INTO students values (0, :studentGroup, :name, :gender, :birthday, :status)");
        $sth->execute([
            'studentGroup' => $group,
            'name' => $firstName . " " . $lastName,
            'gender' => $gender,
            'birthday' => "2000-10-02",
            'status' => 0
        ]);
    }
    catch(PDOException $e) {
        $errors = [];
        $errors[] = "такий студент вже існує";
        echo json_encode(["hasAnyErrors" => true,
                         "errors" => $errors]);
        exit();
    }
    return ["answer" => true, "newID" => $db->lastInsertID(), "hasAnyErrors" => false];
}
function updateStudent($id, $group, $firstName, $lastName, $birthday, $gender)
{
    try {
        global $db;
        $sth = $db->prepare("UPDATE students 
        SET studentGroup = :studentGroup 
        WHERE id = :id");
        $sth->execute([
            'studentGroup' => $group,
            'id' => $id
        ]);

        $sth = $db->prepare("UPDATE students 
        SET gender = :gender 
        WHERE id = :id");
        $sth->execute([
            'gender' => $gender,
            'id' => $id
        ]);

        $sth = $db->prepare("UPDATE students 
        SET name = :name 
        WHERE id = :id");
        $sth->execute([
            'name' => $firstName . " " . $lastName,
            'id' => $id
        ]);

        $sth = $db->prepare("UPDATE students 
        SET birthday = :birthday 
        WHERE id = :id");
        $sth->execute([
            'birthday' => $birthday,
            'id' => $id
        ]);

        $sth = $db->prepare("UPDATE students 
        SET status = :status
        WHERE id = :id");
        $sth->execute([
        'status' => 0,
            'id' => $id
        ]);
    }
    catch(PDOException $e) {
        $errors = [];
        $errors[] = "такий студент вже існує";
        echo json_encode(["hasAnyErrors" => true,
                         "errors" => $errors]);
        exit();
    }
    return ["hasAnyErrors" => false];
}
?>