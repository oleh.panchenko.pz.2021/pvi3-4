<?php 

    header("Content-Type: application/json");
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: Content-Type");
    header("Access-Control-Allow-Credentials: true");

    $name = $_GET["name"];
    $pass = $_GET["pass"];

    $dbHost = "localhost";
    $dbName = "usersandpass";
    $userName = "root";
    $passDB = "";

    $db = new PDO("mysql:host=$dbHost; dbname=$dbName", $userName, $passDB);
    $data = $db->query("SELECT * FROM login");

    $answer = false;

    foreach($data as $record)
    {
        if($name == $record["user"] && $pass == $record["pass"])
        {
            $answer = true;
        }
    }

    echo json_encode(["answer" => $answer]);
?>
