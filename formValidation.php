<?php 
function validateStudent($group, $firstName, $lastName, $date, $gender)
{
    $errors = [];
    if(empty($group)) {
        $errors[] = "group is required";
    }
    if(empty($firstName)) {
        $errors[] = "firstName is required";
    }
    if(empty($lastName)) {
        $errors[] = "lastName is required";
    }
    if(empty($date)) {
        $errors[] = "date is required";
    }
    if(empty($gender)) {
        $errors[] = "gender is required";
    }

    if(!preg_match("/^[a-zA-Z]+$/", $firstName)) {
        $errors[] = "only alphabet in firstName";
    }
    if(!preg_match("/^[a-zA-Z]+$/", $lastName)) {
        $errors[] = "only alphabet in lastName";
    }

    $res;
    if(!empty($errors)) {
        $res = [
            "hasAnyErrors" => true,
            "errors" => $errors
        ];
        echo json_encode($res);
        exit();
    }
    else{
        $res = [
            "hasAnyErrors" => false
        ];
    }

    return $res;
}
?>
