<?php 
    $dbHost = "localhost";
    $dbName = "usersandpass";
    $userName = "root";
    $pass = "";

    try {
        $db = new PDO("mysql:host=$dbHost; dbname=$dbName", $userName, $pass);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e) {
        $errors = [];
        $errors[] = "проблема при підключенні до ДБ";
        echo json_encode(["hasAnyErrors" => true,
                         "errors" => $errors]);
        exit();
    }
?>
