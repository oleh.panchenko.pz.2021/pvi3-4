<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
header("Access-Control-Allow-Credentials: true");

$query = $_GET["query"];

include "dbConnection.php";
include "modelWorkFunctionsWithDB.php";
include "formValidation.php";

if($query == "getStudents")
{
    echo json_encode(getStudentsData());
}
else if($query == "deleteStudent")
{
    $id = $_GET["id"];
    echo json_encode(deleteStudentByID($id));
}
else if($query == "insertStudent")
{
    $group = $_GET["group"];
    $firstName = $_GET["firstName"];
    $lastName = $_GET["lastName"];
    $birthday = $_GET["birthday"];
    $gender = $_GET["gender"];
    validateStudent($group, $firstName, $lastName, $birthday, $gender);
    echo json_encode(insertStudent($group, $firstName, $lastName, $birthday, $gender));
}
else if($query == "updateStudent")
{
    $id = $_GET["id"];
    $group = $_GET["group"];
    $firstName = $_GET["firstName"];
    $lastName = $_GET["lastName"];
    $birthday = $_GET["birthday"];
    $gender = $_GET["gender"];
    validateStudent($group, $firstName, $lastName, $birthday, $gender);
    echo json_encode(updateStudent($id, $group, $firstName, $lastName, $birthday, $gender));
}
?>